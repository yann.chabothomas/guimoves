<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="./"><img src="images/logo.png" alt="Logo"></a>
            <a class="navbar-brand hidden" href="./"><img src="images/logo2.png" alt="Logo"></a>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="index.php"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                </li>

                <h3 class="menu-title">Utilisateurs</h3><!-- /.menu-title -->
                <li>
                    <a href="users.php"> <i class="menu-icon fa fa-users"></i>Liste des Utilisateurs</a>
                </li>

                <h3 class="menu-title">Épreuves</h3><!-- /.menu-title -->
                <li>
                    <a href="enigmes.php"> <i class="menu-icon fa fa-list-ul"></i>Liste des Énigmes</a>
                </li>
                <h3 class="menu-title">Autre</h3>
                <li>
                    <a href="profil.php"> <i class="menu-icon fa fa-eye"></i>Profil </a>
                </li>
                <li>
                    <a href="logout.php"> <i class="menu-icon fa fa-arrow-circle-o-right"></i>Déconnexion </a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside><!-- /#left-panel -->